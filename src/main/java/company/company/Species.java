package company.company;

public enum Species {
    UNKNOWN(8), FISH(0), DOG(1), CAT(2), ROBOCAT(3), DOMESTICAT(4);
    private int value;

    private Species(int value) {
        this.value = value;
    }

}

import company.company.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

public class AddChildrenTest {

    @Test
    void yoxlaSum() {


        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        Set<String>  habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Pet pet1 = new Pet("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        Pet pet2 = new Pet("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.Monday), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.Tuesday), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.Wednesday), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.Friday), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.Saturday), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.Sunday), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.Tuesday), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

        Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet2);
        pets.add(pet1);


        Family family = new Family(father1, children, mother1, pets);


        int firstSizeBeforeAdd = children.size();
        family.addChild(child3);
        int secondtSizeAfterAdd = children.size();

        Assertions.assertTrue(firstSizeBeforeAdd < secondtSizeAfterAdd);


    }
}

